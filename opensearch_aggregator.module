<?php
/**
 * @file
 * Allows searching multiple sites at once using OpenSearch.
 */

/**
 * Implementation of hook_help().
 */
function opensearch_aggregator_help($path = '', $arg) {
  switch ($path) {
    case 'admin/modules#description':
      return t('Allows searching multiple sites at once using OpenSearch.');
  }
}

/**
 * Implementation of hook_menu().
 */
function opensearch_aggregator_menu() {
  $access = array('administer opensearch aggregator');

  $items['admin/settings/opensearch_aggregator'] = array(
    'title' => 'Opensearch Aggregator',
    'description' => 'Create nodes from opensearch feeds.',
    'page callback' => 'opensearch_aggregator_admin',
    'access arguments' => $access);

  $items['admin/settings/opensearch_aggregator/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
    'access arguments' => $access);

  $items['admin/settings/opensearch_aggregator/add/source'] = array(
    'title' => 'Add source',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('opensearch_aggregator_form_source'),
    'access arguments' => $access,
    'type' => MENU_LOCAL_TASK);

  $items['admin/settings/opensearch_aggregator/edit/source/%'] = array(
    'title' => 'edit source',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('opensearch_aggregator_form_source', 5),
    'access arguments' => $access,
    'type' => MENU_CALLBACK);
  return $items;
}

/**
 * Implementation of hook_perms().
 */
function opensearch_aggregator_perm() {
  return array('administer opensearch aggregator');
}

/**
 * Implementation of hook_cron().
 */
function opensearch_aggregator_cron() {
  // Clear searches older than 30 minutes
  db_query('DELETE FROM {opensearch_aggregator_total} WHERE access < %d', time() - 1800);
  $result = db_query('SELECT DISTINCT(keywords) FROM {opensearch_aggregator_result} WHERE access < %d', time() - 1800);
  while ($search = db_fetch_object($result)) {
    db_query("DELETE FROM {opensearch_aggregator_result} WHERE keywords = '%s'", $search->keywords);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Search presentation ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Implementation of hook_search().
 */
function opensearch_aggregator_search($op = 'search', $keywords = NULL) {
  switch ($op) {
    case 'name':
      return t('distributed');
    case 'reset':
      return;
    case 'search':
      // Fetch source info.
      $sources = opensearch_aggregator_get_sources();

      // Get pager info.
      global $pager_total, $pager_page_array, $pager_total_items, $locale;
      $page = isset($_GET['page']) ? $_GET['page'] : '';
      $pager_page_array = explode(',', $page);

      // Query sources.
      $items = opensearch_aggregator_query($keywords, max(0, $pager_page_array[0]) * 10, 10);

      // Format results.
      $results = array();
      foreach ($items as $item) {
        $results[] = array(
          'link' => $item->link,
          'title' => $item->title,
          'snippet' => $item->description,
          'score' => $item->score,
          'extra' => array(t('Source site: !link', array('!link' => l($sources[$item->sid]->title, $sources[$item->sid]->link, array(), NULL, NULL, FALSE, TRUE)))),
        );
      }

      // Fill in totals.
      list($total) = opensearch_aggregator_get_totals($keywords);
      $pager_total_items[0] = $total;
      $pager_total[0] = ceil($total / 10);
      $pager_page_array[0] = max(0, min((int)$pager_page_array[0], ((int)$total) - 1));


      return $results;
  }
}

/**
 * Take a set of result bins and aggregate them.
 *
 * We stop as soon as one bin empties, as this is the
 * last result we can reliably sort. The only exception
 * is when we have all results.
 */
function opensearch_aggregator_sort($cache, $keywords, $limit) {
  // Check for empty bins.
  if (count($cache) == 0) {
    return array();
  }

  // Get totals and see if we have any complete bins
  list(, $totals) = opensearch_aggregator_get_totals($keywords);
  $complete = array();
  foreach ($cache as $sid => $bin) {
    $complete[$sid] = ($totals[$sid] == count($bin));
  }

  // Aggregate results up to desired limit.
  $results = array();
  $k = 0;
  while (count($cache) && count($results) < $limit) {
    // Find highest ranking element.
    $max = $val = -1;
    foreach ($cache as $sid => $bin) {
      $item = reset($bin);
      if ($item->score > $val) {
        $val = $item->score;
        $max = $sid;
      }
    }

    // Keep the element and remove it from the bin.
    $results[] = array_shift($cache[$max]);

    // Check for empty bins
    // If a bin is emptied, stop, unless the bin was complete.
    if (count($cache[$max]) == 0) {
      if (!$complete[$max]) {
        return $results;
      }
      else {
        unset($cache[$max]);
      }
    }
  }

  return $results;
}

/**
 * Query the OpenSearch aggregator for results.
 */
function opensearch_aggregator_query($keywords, $offset, $limit) {
  $sources = opensearch_aggregator_get_sources();
  if (count($sources) == 0) return array();

  $results = NULL;
  $last_size = -1;

  // See if we have fulfilled the query.
  while (is_null($results) || count($results) < $offset + $limit) {
    // Only fetch if the cache has been checked.
    if (is_array($results)) {
      // Check if we have more results than before.
      $size = count($results);
      if ($last_size == $size) {
        break;
      }
      $last_size = $size;

      // Fetch more results from all sources.
      foreach ($sources as $sid => $source) {
        opensearch_aggregator_fetch($source, $keywords, count($cache[$sid]), $limit);
      }
    }

    // Build the results based on our cache.
    $cache = opensearch_aggregator_get_cache($keywords);
    $results = opensearch_aggregator_sort($cache, $keywords, $offset + $limit);
  }

  // Return results
  return array_slice($results, $offset, $limit);
}

/**
 * Fetch the cached results for a particular query.
 */
function opensearch_aggregator_get_cache($keywords) {
  // Fetch results and return them ordered, grouped by sid.
  $cache = array();
  $result = db_query("SELECT * FROM {opensearch_aggregator_result} WHERE keywords = '%s' ORDER BY sid, offset", $keywords);
  while ($item = db_fetch_object($result)) {
    $cache[$item->sid][$item->offset] = $item;
  }

  return $cache;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Feed fetching and parsing //////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Checks a news feed for new items.
 */
function opensearch_aggregator_fetch(&$source, $keywords, $offset, $limit) {
  global $channel, $image, $locale;

  // Substitute feed parameters
  // TODO: parse all identifiers
  $url = strtr($source->feed, array(
    '{searchTerms}' => drupal_urlencode($keywords),
    '{count}' => (int)$limit,
    '{startPage}' => 1 + (int)($offset / (((int)$source->per_page) ? $source->per_page : $limit)),
    '{startIndex}' => 1 + (int)$offset,
    '{language}' => $locale,
    '{outputEncoding}' => 'UTF-8',
  ));

  // Request feed.
  $result = drupal_http_request($url);

  // Process HTTP response code.
  switch ($result->code) {
    case 200:
    case 302:
    case 307:
      // Filter the input data:
      if (opensearch_aggregator_parse_feed($result->data, $source, $keywords)) {
        break;
      }
    default:
      watchdog('aggregator', 'The OpenSearch feed from %site seems to be broken, due to "%error".', array('%site' => $source->title, '%error' => $result->code .' '. $result->error), WATCHDOG_WARNING);
      drupal_set_message(t('The OpenSearch feed from %site seems to be broken, because of error "%error".', array('%site' => $source->title, '%error' => $result->code .' '. $result->error)));
  }
}

/**
 * Call-back function used by the XML parser.
 */
function opensearch_aggregator_element_start($parser, $name, $attributes) {
  global $item, $element, $tag, $items, $channel;

  switch ($name) {
    case 'IMAGE':
    case 'TEXTINPUT':
    case 'CONTENT':
    case 'SUMMARY':
    case 'TAGLINE':
    case 'SUBTITLE':
    case 'LOGO':
    case 'INFO':
      $element = $name;
      break;
    case 'ID':
      if ($element != 'ITEM') {
        $element = $name;
      }
    case 'LINK':
      if ($attributes['REL'] == 'alternate') {
        if ($element == 'ITEM') {
          $items[$item]['LINK'] = $attributes['HREF'];
        }
        else {
          $channel['LINK'] = $attributes['HREF'];
        }
      }
      break;
    case 'ITEM':
      $element = $name;
      $item += 1;
      break;
    case 'ENTRY':
      $element = 'ITEM';
      $item += 1;
      break;
  }

  $tag = $name;
}

/**
 * Call-back function used by the XML parser.
 */
function opensearch_aggregator_element_end($parser, $name) {
  global $element;

  switch ($name) {
    case 'IMAGE':
    case 'TEXTINPUT':
    case 'ITEM':
    case 'ENTRY':
    case 'CONTENT':
    case 'INFO':
      $element = '';
      break;
    case 'ID':
      if ($element == 'ID') {
        $element = '';
      }
  }
}

/**
 * Call-back function used by the XML parser.
 */
function opensearch_aggregator_element_data($parser, $data) {
  global $channel, $element, $items, $item, $image, $tag;
  switch ($element) {
    case 'ITEM':
      $items[$item][$tag] .= $data;
      break;
    case 'IMAGE':
    case 'LOGO':
      $image[$tag] .= $data;
      break;
    case 'LINK':
      if ($data) {
        $items[$item][$tag] .= $data;
      }
      break;
    case 'CONTENT':
      $items[$item]['CONTENT'] .= $data;
      break;
    case 'SUMMARY':
      $items[$item]['SUMMARY'] .= $data;
      break;
    case 'TAGLINE':
    case 'SUBTITLE':
      $channel['DESCRIPTION'] .= $data;
      break;
    case 'INFO':
    case 'ID':
    case 'TEXTINPUT':
      // The sub-element is not supported. However, we must recognize
      // it or its contents will end up in the item array.
      break;
    default:
      $channel[$tag] .= $data;
  }
}

/**
 * Parse the W3C date/time format, a subset of ISO 8601. PHP date parsing
 * functions do not handle this format.
 * See http://www.w3.org/TR/NOTE-datetime for more information.
 * Originally from MagpieRSS (http://magpierss.sourceforge.net/).
 *
 * @param $date_str A string with a potentially W3C DTF date.
 * @return A timestamp if parsed successfully or -1 if not.
 */
function opensearch_aggregator_parse_w3cdtf($date_str) {
  if (preg_match('/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2})(:(\d{2}))?(?:([-+])(\d{2}):?(\d{2})|(Z))?/', $date_str, $match)) {
    list($year, $month, $day, $hours, $minutes, $seconds) = array($match[1], $match[2], $match[3], $match[4], $match[5], $match[6]);
    // calc epoch for current date assuming GMT
    $epoch = gmmktime($hours, $minutes, $seconds, $month, $day, $year);
    if ($match[10] != 'Z') { // Z is zulu time, aka GMT
      list($tz_mod, $tz_hour, $tz_min) = array($match[8], $match[9], $match[10]);
      // zero out the variables
      if (!$tz_hour) {
        $tz_hour = 0;
      }
      if (!$tz_min) {
        $tz_min = 0;
      }
      $offset_secs = (($tz_hour * 60) + $tz_min) * 60;
      // is timezone ahead of GMT?  then subtract offset
      if ($tz_mod == '+') {
        $offset_secs *= -1;
      }
      $epoch += $offset_secs;
    }
    return $epoch;
  }
  else {
    return FALSE;
  }
}

function opensearch_aggregator_parse_feed(&$data, &$source, $keywords) {
  global $items, $image, $channel;

  // Unset the global variables before we use them.
  unset($GLOBALS['element'], $GLOBALS['item'], $GLOBALS['tag']);
  $items = array();
  $image = array();
  $channel = array();

  // Prepare XML parser.
  $xml_parser = drupal_xml_parser_create($data);
  xml_set_element_handler($xml_parser, 'opensearch_aggregator_element_start', 'opensearch_aggregator_element_end');
  xml_set_character_data_handler($xml_parser, 'opensearch_aggregator_element_data');

  // Extract XML base.
  $xml_base = $source->feed;
  if (preg_match('@xml:base=(?:["\']([^"\']+)|([^ ]+))@', $result->data, $matches)) {
    list(, $quoted, $unquoted) = $matches;
    $xml_base = $quoted . $unquoted;
  }
  $xml_base = parse_url($xml_base);
  unset($xml_base['query']);
  unset($xml_base['fragment']);

  // Parse data.
  if (!xml_parse($xml_parser, $data, 1)) {
    watchdog('aggregator', 'The OpenSearch feed from %site seems to be broken, due to an error "%error" on line %line.', array('%site' => $source->title, '%error' => xml_error_string(xml_get_error_code($xml_parser)), '%line' => xml_get_current_line_number($xml_parser)), WATCHDOG_WARNING);
    drupal_set_message(t('The OpenSearch feed from %site seems to be broken, because of error "%error" on line %line.', array('%site' => $source->title, '%error' => xml_error_string(xml_get_error_code($xml_parser)), '%line' => xml_get_current_line_number($xml_parser))), 'error');
    return 0;
  }
  xml_parser_free($xml_parser);

  /*
  ** Prepare the channel data.
  */
  foreach ($channel as $key => $value) {
    $channel[$key] = trim($value);
  }

  // Store items per page for page-based offsets.
  if ((int)$channel['OPENSEARCH:ITEMSPERPAGE']) {
    $source->per_page = (int)$channel['OPENSEARCH:ITEMSPERPAGE'];
    db_query("UPDATE {opensearch_aggregator_source} SET per_page = %d WHERE sid = %d", $source->per_page, $source->sid);
  }

  // Get starting index.
  $offset = isset($channel['OPENSEARCH:STARTINDEX']) ? $channel['OPENSEARCH:STARTINDEX'] : 1;

  // Keep track of totals.
  if ($channel['OPENSEARCH:TOTALRESULTS']) {
    db_query("DELETE FROM {opensearch_aggregator_total} WHERE sid = %d AND keywords = '%s'", $source->sid, $keywords);
    db_query("INSERT INTO {opensearch_aggregator_total}
              (sid, keywords, total, access) VALUES
              (%d, '%s', %d, %d)",
              $source->sid, $keywords, $channel['OPENSEARCH:TOTALRESULTS'], time());
  }

  // Update source link
  if ($channel['LINK']) {
    $source->link = $channel['LINK'];
    db_query("UPDATE {opensearch_aggregator_source} SET link = '%s' WHERE sid = %d", $source->link, $source->sid);
  }

  /*
  ** Update the feed data.
  */
  foreach ($items as $item) {
    unset($title, $link, $author, $description);

    // Prepare the item:
    foreach ($item as $key => $value) {
      $item[$key] = trim($value);
    }

    /*
    ** Resolve the item's title.  If no title is found, we use
    ** up to 40 characters of the description ending at a word
    ** boundary but not splitting potential entities.
    */

    if ($item['TITLE']) {
      $title = $item['TITLE'];
    }
    else {
      $title = preg_replace('/^(.*)[^\w;&].*?$/', "\\1", truncate_utf8($item['DESCRIPTION'], 40));
    }

    /*
    ** Resolve the item's link.
    */

    if ($item['LINK']) {
      $link = $item['LINK'];
    }
    elseif ($item['GUID'] && (strncmp($item['GUID'], 'http://', 7) == 0)) {
      $link = $item['GUID'];
    }
    else {
      $link = $source->osd;
    }
    $link = array_merge($xml_base, parse_url($link));
    $url = $link['scheme'] .'://'. $link['host'] . $link['path'];
    if (isset($link['query'])) $url .= '?'. $link['query'];
    if (isset($link['fragment'])) $url .= '#'. $link['fragment'];

    /**
     * Atom feeds have a CONTENT and/or SUMMARY tag instead of a DESCRIPTION tag
     */
    if ($item['CONTENT:ENCODED']) {
      $item['DESCRIPTION'] = $item['CONTENT:ENCODED'];
    }
    else if ($item['SUMMARY']) {
      $item['DESCRIPTION'] = $item['SUMMARY'];
    }
    else if ($item['CONTENT']) {
      $item['DESCRIPTION'] = $item['CONTENT'];
    }

    // Resolve the score
    $score = isset($item['RELEVANCE:SCORE']) ? $item['RELEVANCE:SCORE'] : 0;

    // Delete existing record.
    db_query("DELETE FROM {opensearch_aggregator_result} WHERE sid = %d AND keywords = '%s' AND offset = %d", $source->sid, $keywords, $offset);

    // Insert new record.
    db_query("INSERT INTO {opensearch_aggregator_result}
              (sid, keywords, offset, score, title, link, description, access) VALUES
              (%d, '%s', %d, %f, '%s', '%s', '%s', %d)",
              $source->sid, $keywords, $offset, $score, $title, $url, $item['DESCRIPTION'], time());

    // Move to next item.
    ++$offset;
  }


  return 1;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
// Admin interface ////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Menu callback
 */
function opensearch_aggregator_admin() {
  $result = db_query('SELECT * FROM {opensearch_aggregator_source} ORDER BY title');

  $output .= '<h3>'. t('Source overview') .'</h3>';

  $header = array(t('Title'), t('URL'), array('data' => t('Operations'), 'colspan' => '1'));
  $rows = array();
  while ($source = db_fetch_object($result)) {
    $rows[] = array(
      check_plain($source->title),
      check_url(url($source->osd)) .'<br /><small>'. check_url(url($source->feed)) .'</small>',
      l(t('edit'), "admin/settings/opensearch_aggregator/edit/source/$source->sid"));
  }
  if (count($rows) == 0) {
    $rows[] = array(array('colspan' => 3, 'data' => t('No sources defined.')));
  }
  $output .= theme('table', $header, $rows);

  return $output;
}

/**
 * Generate a form to add/edit feed sources.
 */
function opensearch_aggregator_form_source($form_state, $nid = 0) {
  $edit = opensearch_aggregator_get_source($nid);

  $form['title'] = array('#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $edit['title'],
    '#maxlength' => 64,
    '#description' => t('The name of the source; typically the name of the web site you syndicate content from.'),
    '#required' => TRUE,
  );
  $form['osd'] = array('#type' => 'textfield',
    '#title' => t('Description URL'),
    '#default_value' => $edit['osd'],
    '#maxlength' => 255,
    '#description' => t('The fully-qualified URL of the OpenSearch Description file or a website that exposes it in its meta tags.'),
    '#required' => TRUE,
  );
  $form['feed'] = array('#type' => 'value');

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  if ($edit['sid']) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['sid'] = array('#type' => 'hidden', '#value' => $edit['sid']);
  }

  return $form;
}

/**
 * Validate opensearch_aggregator_form_source form submissions.
 */
function opensearch_aggregator_form_source_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Submit')) {
    // Check for duplicate titles
    if (isset($form_state['values']['sid'])) {
      $result = db_query("SELECT title, osd FROM {opensearch_aggregator_source} WHERE (title = '%s' OR osd = '%s') AND sid <> %d", $form_state['values']['title'], $form_state['values']['osd'], $form_state['values']['sid']);
    }
    else {
      $result = db_query("SELECT title, osd FROM {opensearch_aggregator_source} WHERE title = '%s' OR osd = '%s'", $form_state['values']['title'], $form_state['values']['osd']);
    }
    while ($source = db_fetch_object($result)) {
      if (strcasecmp($source->title, $form_state['values']['title']) == 0) {
        form_set_error('title', t('A source named %source already exists. Please enter a unique title.', array('%source' => $form_state['values']['title'])));
      }
    }
    
    $discover = opensearch_discover($form_state['values']['osd']);
    if ($discover['success']) {
      form_set_value($form['feed'], $discover['template'], $form_state);
    }
    else {
      form_set_error('osd', t('Could not find a valid OpenSearch Description at the specified URL. The error was: %error', array('%error' => $discover['error'])));
    }
  }
}

/**
 * Perform autodiscovery for an OpenSearch description at the given URL.
 *
 * This function acceps both a direct link to an OSD file as well as an HTML
 * page that includes a <link rel="search"> tag in its header.
 *
 * @param $url
 *   The URL to inspect.
 * @return
 *   A tuple with:
 *   'success': Whether autodiscovery was successful.
 *   'template': A URL template for the OpenSearch if successful.
 *   'error': An error message if failed.
 */
function opensearch_discover($url) {
  // Request URL.
  $result = drupal_http_request($url);
  // Process HTTP response code.
  switch ($result->code) {
    case 301:
      // Redirect.
      return opensearch_discover($result->redirect_url);

    case 200:
    case 302:
    case 307:
      // TODO: Allow autodiscovery and parsing of OSD file directly or through HTML.

      if ($result->data) {
        // Find URL template for RSS in OSD file.
        preg_match_all('@<Url[^>]+template=(?:["\']([^"\']+)|([^ ]+))[^>]*@', $result->data, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
          list($all, $quoted, $unquoted) = $match;
          if (strpos($all, 'application/rss+xml') !== FALSE) {
            return array('success' => TRUE, 'template' => $quoted . $unquoted);
          }
        }
        
        // Find link tags in HTML HEAD.
        preg_match_all('`<link[^>]+rel=["\']?search["\']?[^>]*>`', $result->data, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
          list($all) = $match;
          if (strpos($all, 'application/opensearchdescription+xml') !== FALSE) {
            if (preg_match('`href=(["\']([^"\']+)["\']|([^ "\])+(?=[ >]))`', $all, $match)) {
              // We have the URL to an OSD file. Fetch it.
              return opensearch_discover($match[2] . $match[3]);
            }
          }
        }
      }
    default:
      return array('success' => FALSE, 'error' => $result->code .' '. $result->error);
  }
}

/**
 * Process opensearch_aggregator_form_source form submissions.
 * @todo Add delete confirmation dialog.
 */
function opensearch_aggregator_form_source_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete')) {
    $title = $form_state['values']['title'];
    // Unset the title:
    unset($form_state['values']['title']);
  }
  opensearch_aggregator_save_source($form_state['values']);
  if (isset($form_state['values']['sid'])) {
    if (isset($form_state['values']['title'])) {
      drupal_set_message(t('The source %source has been updated.', array('%source' => $form_state['values']['title'])));
    }
    else {
      watchdog('opensearch', 'Source %source deleted.', array('%source' => $title));
      drupal_set_message(t('The source %source has been deleted.', array('%source' => $title)));
    }
  }
  else {
    watchdog('opensearch', 'Source %source added.', array('%source' => $form_state['values']['title']), WATCHDOG_NOTICE, l(t('view'), 'admin/aggregator'));
    drupal_set_message(t('The source %source has been added.', array('%source' => $form_state['values']['title'])));
  }
  $form_state['redirect'] = 'admin/settings/opensearch_aggregator';
}

/**
 * Fetch an opensearch source.
 */
function opensearch_aggregator_get_source($sid) {
  return db_fetch_array(db_query('SELECT * FROM {opensearch_aggregator_source} WHERE sid = %d', $sid));
}

/**
 * Add/edit/delete an aggregator source.
 */
function opensearch_aggregator_save_source($edit) {
  if ($edit['sid'] && $edit['title']) {
    db_query("UPDATE {opensearch_aggregator_source} SET title = '%s', osd = '%s', feed = '%s' WHERE sid = %d", $edit['title'], $edit['osd'], $edit['feed'], $edit['sid']);
  }
  else if ($edit['sid']) {
    db_query('DELETE FROM {opensearch_aggregator_total} WHERE sid = %d', $edit['sid']);
    db_query('DELETE FROM {opensearch_aggregator_result} WHERE sid = %d', $edit['sid']);
    db_query('DELETE FROM {opensearch_aggregator_source} WHERE sid = %d', $edit['sid']);
  }
  else if ($edit['title']) {
    db_query("INSERT INTO {opensearch_aggregator_source} (title, osd, feed, per_page) VALUES ('%s', '%s', '%s', 0)", $edit['title'], $edit['osd'], $edit['feed']);
    $edit['sid'] = db_last_insert_id('opensearch_aggregator_source', 'sid');
  }
}

/**
 * Return the list of OpenSearch sources that are configured.
 */
function opensearch_aggregator_get_sources() {
  $sources = array();
  $result = db_query('SELECT * FROM {opensearch_aggregator_source}');
  while ($source = db_fetch_object($result)) {
    $sources[$source->sid] = $source;
  }
  return $sources;
}

/**
 * Return the list of results of a particular query by source.
 */
function opensearch_aggregator_get_totals($keywords) {
  $sources = array();
  $result = db_query("SELECT sid, total FROM {opensearch_aggregator_total} WHERE keywords = '%s'", $keywords);
  $total = 0;
  while ($source = db_fetch_object($result)) {
    $sources[$source->sid] = $source->total;
    $total += $source->total;
  }
  return array($total, $sources);
}

