OpenSearch Aggregator, by Steven Wittens

Note: this is still under development. Do not deploy on production sites.

This module allows you to search multiple OpenSearch feeds at the same time. The results are aggregated and cached locally, and results are fetched incrementally as you page further down the results.

It works best if the search sources are all run on the same software. The OpenSearch module for Drupal can provide the appropriate feeds, and will also pass along relevance information. OpenSearch Aggregator will sort the results by descending relevance globally.

Note that you need to patch node.module with the provided patch for now.

TODO:
- Figure out a smart caching policy. Right now, results are cached forever.
- Fix problems at the end of the pager.
- Add autodiscovery of the Opensearch Description file. i.e. type in the URL to a drupal site, and do the complete autodiscovery from HTML -> OSD -> OS Feed
